# SmartGitLFSDiff

Demonstrates a limitation of SmartGit when trying to use external diff/merge tools with LFS backed text files.

## Setup

1. Install SmartGit
2. Configure SmartGit to use an extermal merge tool (like KDiff3)
3. Make sure SmartGit's low level property `compare.applyGitFilters` is set to true
4. Create a new Git repository and configure LFS tracking:
    ```
    git lfs install
    git lfs track example.txt
    git add .gitattributes
    git commit -m "LFS tracking"
    ```
5. Add example.txt to the repository and commit
    ```
    echo "some data" > example.txt
    git add example.txt
    git commit -m "new file"
    ```
6. Create a new branch and make a change to example.txt
7. Go back to the default branch and make a conflicting change to example.txt
8. Try to merge the branches. This should cause a merge conflict requiring human intervention.
9. In SmartGit choose "Conflict Solver" to launch the external merge tool.
10. Note that the temp files passed to the external diff tool contain the contents of the LFS pointer file instead of the "real" file.

## Notes

* When in a merge conflict state SmartGit's built in diff view also shows the LFS pointers instead of the real file contents.
* Using "Show Changes" in a merge conflict state (which opens the configured diff tool instead of the merge tool) opens the external merge tool with the real contents on the left and the conflicted LFS pointer on the right (e.g. with "<<<<<<< HEAD" markup).
* SmartGit's diff view in the log window correctly expands LFS pointers and shows a diff of the real changes (unlike the diff view in the main window).
